import 'package:flutter/material.dart';

const Color themeColor = Color.fromRGBO(132, 95, 63, 1.0);
const Color floorTitleColor = Color.fromRGBO(51, 51, 51, 1);
const Color searchBarBgColor = Color.fromRGBO(240, 240, 240, 1.0);
const Color searchBarTxtColor = Color(0xFFCDCDCD);
const Color divideLineColor = Color.fromRGBO(245, 245, 245, 1.0);
const Color categoryDefaultColor = Color(0xFF666666);
const Color priceColor = Color.fromRGBO(182, 9, 9, 1.0);
const Color pinweicorverSubtitleColor = Color.fromRGBO(153, 153, 153, 1.0);
const Color pinweicorverBtbgColor = themeColor;
const Color tabtxtColor = Color.fromRGBO(88, 88, 88, 1.0);
const Color cartDisableColor = Color.fromRGBO(221, 221, 221, 1.0);
const Color cartItemChangenumBtColor = Color.fromRGBO(153, 153, 153, 1.0);
const Color cartItemCountTxtColor = Color.fromRGBO(102, 102, 102, 1.0);
const Color cartBottomBgColor = Color(0xFFFFFFFF);
const Color goPayBtBgColor = themeColor;
const Color goPayBtTxtColor = Color(0xFFFFFFFF);
const Color searchAppBarBgColor = Color(0xFFFFFFFF);
const Color bottomBarbgColor = Color.fromRGBO(250, 250, 250, 1.0);
const Color searchBarTextBoxBgColorHome = Color.fromRGBO(34, 34, 34, 0.05);
const Color searchRecomendDividerColor = Color(0xFFdedede);
const COLOR_DISABLED = Color(0xFFCCCCCC);

// Color for theme
const CL_DISABLED = Color(0xFFCCCCCC);
const CL_BG = Color(0xFFEFEFEF);
const CL_WHITE = Colors.white;
const CL_PRIMARY_BTN_BG = Color(0xFFF8CF68);
const CL_SECONDARY_BTN_BG = Color(0xFFDDDDDD);
const CL_PRIMARY_DARK = Color(0xFF222831);
const CL_SECONDARY_DARK = Color(0xFF393e46);
const CL_ACCENT_DARK = Color(0xFF2EF4E3);
const CL_ACCENT_LIGHT = Colors.blue;

// Main
const Color PRIMARY_COLOR = Color(0xFFED1C24);
const Color PRIMARY_COLOR_TTS = Color(0xFF5580df);
const Color SUCCESS_COLOR = Color(0xFF22b55d);

// Appbar
const Color appBarIconColor = Colors.white;
const Color appBarDetailTitleColor = Colors.white;

// Tabbar
const Color tabBarTextColor = Colors.white;
const Color tabBarTextLabelColor = Color(0xFF999999);

// Home
const Color cardNewsBackgroundColor = Color(0xFFFAFAFC);
const Color cardNewsBorderColor = Color(0xFFF2F2F2);
const Color topicLargeType4Color = Color(0xFF226ac5);
const Color timelargeType4Color = Color(0xFF226ac5);
const Color dividerColor = Color(0xFFDCDCDC);
const Color voteBackgroundColor = Color(0xFFE7EFFA);
const Color buttonVoteColor = Color(0xFF5580df);
const Color textVoteColor = Color(0xFF8f8f8f);
const Color textPositionBoxTopViewColor = Color(0xFFed1c24);
const Color popupBuyStarColor = Color(0xFF5580df);
const Color dotSliderColor = Color(0xFFd9d9d9);

// Detail
const Color detailDistributionDateColor = Color(0xFF999999);
const Color CL_CIRCLE_BORDER = Color(0xFFb6d7ff);
