import 'package:flutter/material.dart';
import 'package:qr_event/const/color.dart';
import 'package:qr_event/model/tick.dart';
import 'package:qr_event/widgets/button_elevated_custom.dart';

class TickInformation extends StatelessWidget {
  const TickInformation({Key key, this.onGiftStar, this.tick})
      : super(key: key);

  final ValueChanged<int> onGiftStar;
  final Tick tick;

  @override
  Widget build(BuildContext context) {
    final _styleDis = TextStyle(
      fontStyle: FontStyle.italic,
      color: Colors.black,
    );
    final _styleMain = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 18,
      fontStyle: FontStyle.normal,
    );

    final _tickIsTrue = (tick.checkTickIsTrue);
    final _color = _tickIsTrue
        ? tick?.data?.getStatusColor?.withOpacity(0.9)
        : PRIMARY_COLOR;

    return AlertDialog(
      titlePadding: EdgeInsets.zero,
      contentPadding: EdgeInsets.all(16.0),
      insetPadding: EdgeInsets.all(16.0),
      title: AppBar(
        backgroundColor: _color,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text('Thông tin vé'),
        actions: [
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.clear, color: CL_WHITE),
          )
        ],
      ),
      content: _tickIsTrue
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                // Name
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text('Tên : ', style: _styleDis),
                      ),
                      Expanded(
                        flex: 7,
                        child: Text(
                          ' ${tick?.data?.name ?? ''}',
                          style: _styleMain,
                        ),
                      ),
                    ],
                  ),
                ),

                // Email
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text('Email : ', style: _styleDis),
                      ),
                      Expanded(
                        flex: 7,
                        child: Text(
                          ' ${tick?.data?.email ?? ''}',
                          style: _styleMain,
                        ),
                      ),
                    ],
                  ),
                ),

                // Phone
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text('Sđt : ', style: _styleDis),
                      ),
                      Expanded(
                        flex: 7,
                        child: Text(
                          ' ${tick?.data?.phone ?? ''}',
                          style: _styleMain,
                        ),
                      ),
                    ],
                  ),
                ),

                // Status
                Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                  child: Row(
                    children: [
                      Expanded(
                          flex: 3,
                          child: Text(
                            'Trạng thái : ',
                            style: _styleDis,
                          )),
                      Expanded(
                        flex: 7,
                        child: Text(
                          ' ${tick?.data?.getStatus?.toUpperCase() ?? ''}',
                          style: _styleMain.copyWith(
                            color: _color,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(width: double.maxFinite)
              ],
            )
          : Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(width: double.maxFinite),
                Center(
                  child: Text(tick?.error ?? ''),
                ),
                IconButton(
                  iconSize: 40,
                  onPressed: () {},
                  icon: Icon(Icons.error),
                  color: PRIMARY_COLOR,
                )
              ],
            ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ButtonElevatedCustom(
                color: _color,
                width: 150,
                text: "Xong",
                onTap: () => Navigator.pop(context),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
