import 'package:qr_event/const/bool.dart';

class Apis {
  // Gobal

  static const String sso_beta = 'https://beta-sso.tuoitre.vn';
  static const String sso_live = 'https://sso.tuoitre.vn';
  static const String ss5 = 'https://s5.tuoitre.vn';

  static const String broker_name_beta = 'beta-client';
  static const String broker_name_live = 'app-tuoitre';

  // Get
  bool get ENV => LIVE_ENV;
  String get sso_url => ENV ? sso_live : sso_beta;

  // SSO
  String get checkTicket => '$sso_url/api/v1/scan-ticket-app';
}
