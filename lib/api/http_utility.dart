import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart';
import 'package:qr_event/base/app_helpers.dart';
import 'package:qr_event/const/bool.dart';
import 'package:qr_event/const/length.dart';

class Request {
  const Request({
    this.urlApi,
    this.method = Method.GET,
    this.params,
    this.headers,
    this.auth,
  });

  // StartPoint and EndPoint, Param
  final String urlApi;
  final dynamic params;
  final Method method;
  final Map<String, String> headers;
  final Map<String, String> auth;
}

enum StatusRequest { SUCCESS, FAILURE, TIMEOUT }

enum Method { GET, POST, PUT, DELETE }

class ResponseModel {
  final dynamic data;
  final String message;
  final int statusCode;
  final StatusRequest status;

  const ResponseModel({
    this.statusCode = 200,
    this.status = StatusRequest.FAILURE,
    this.message,
    this.data,
  });

  factory ResponseModel.fromJson(Map<String, dynamic> jsonData) {
    if (jsonData == null || (jsonData?.isEmpty ?? false)) {
      return null;
    }

    return ResponseModel(
      data: lookup<dynamic>(jsonData, ['data']),
      message: lookup<String>(jsonData, ['message']),
      statusCode: lookup<int>(jsonData, ['code']),
      status: lookup<String>(jsonData, ['data', 'status']) == "false"
          ? StatusRequest.FAILURE
          : StatusRequest.SUCCESS,
    );
  }
}

class Service {
  Client _client = Client();

  Future<dynamic> getRequest(Request request, {bool isSsoApi = false}) async {
    try {
      final response = await _client
          .get(
            Uri.tryParse(request.urlApi),
            headers: request.headers,
          )
          .timeout(REQUEST_TIME_OUT);

      // Print api url
      printDebug('GET : ${request.urlApi}');

      // Is show body
      if (IS_SHOW_DEBUG_RESPONSE) {
        printDebug('BODY : ${json.decode(response.body)}');
      }

      // Check sso api
      if (isSsoApi) {
        return json.decode(response.body);
      } else {
        return (response.statusCode == 200)
            ? json.decode(response.body)
            : throw Exception('Failed to Load');
      }
    } on TimeoutException catch (_) {
      printDebug("Error : TimeoutException");
    } catch (error) {
      printDebug("Error : $error");
    }

    return null;
  }

  Future<dynamic> postRequest(Request request, {bool isSsoApi = false}) async {
    try {
      final response = await _client
          .post(
            Uri.tryParse(request.urlApi),
            headers: request.headers,
            body: request.params,
          )
          .timeout(REQUEST_TIME_OUT);

      // Print api url
      printDebug('POST : ${request.urlApi}');

      // Is show body
      if (IS_SHOW_DEBUG_RESPONSE) {
        printDebug('BODY : ${json.decode(response.body)}');
      }

      // Check sso api
      if (isSsoApi) {
        return json.decode(response.body);
      } else {
        return (response.statusCode == 200)
            ? json.decode(response.body)
            : throw Exception('Failed to Load');
      }
    } on TimeoutException catch (_) {
      printDebug("Error : TimeoutException");
    } catch (error) {
      printDebug("Error : $error");
    }

    return null;
  }
}
