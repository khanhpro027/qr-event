// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:qr_code_scanner/qr_code_scanner.dart';
// import 'package:qr_event/base/app_helpers.dart';
// import 'package:qr_event/const/color.dart';
// import 'package:qr_event/widgets/button_elevated_custom.dart';

// class QRViewScreen extends StatefulWidget {
//   const QRViewScreen({Key key, this.onTextResult}) : super(key: key);

//   final ValueChanged<String> onTextResult;

//   @override
//   State<StatefulWidget> createState() => _QRViewScreenState();
// }

// class _QRViewScreenState extends State<QRViewScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Column(
//         children: <Widget>[
//           Container(
//             margin: const EdgeInsets.all(16),
//             child: ButtonElevatedCustom(
//               height: 60,
//               color: PRIMARY_COLOR,
//               text: getTextButton.toUpperCase(),
//               onTap: () async {
//                 if (_isPause) {
//                   await controller?.resumeCamera();
//                 } else {
//                   await controller?.pauseCamera();
//                 }

//                 setState(() {
//                   _isPause = !_isPause;
//                 });
//               },
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
