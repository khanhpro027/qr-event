import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:qr_event/base/app_helpers.dart';
import 'package:qr_event/base/app_theme.dart';
import 'package:qr_event/const/bool.dart';
import 'package:qr_event/home.dart';

Future<void> main() async {
  // add this, and it should be the first line in main method
  WidgetsFlutterBinding.ensureInitialized();

  // Check flavor
  final String flavor =
      await const MethodChannel('flavor').invokeMethod<String>('getFlavor');

  printDebug('STARTED WITH FLAVOR $flavor');

  switch (flavor) {
    case 'staging':
      FlavorConfig(
        name: 'Staging',
        color: Colors.red,
      );
      LIVE_ENV = false;
      break;

    default:
      FlavorConfig();
      LIVE_ENV = true;
  }

  // Set orientation
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]).then((_) {
    runApp(
      MaterialApp(
        theme: AppTheme().currentTheme,
        debugShowCheckedModeBanner: false,
        home: const Home(),
      ),
    );
  });
}
