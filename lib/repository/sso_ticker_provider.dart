import 'dart:async';

import 'package:qr_event/api/apis.dart';
import 'package:qr_event/api/http_utility.dart';
import 'package:qr_event/base/app_helpers.dart';
import 'package:qr_event/model/tick.dart';

class SSOTickerProvider {
  // SSO check ticker
  Future<bool> checkTicker({
    String id = '5e3ce3162f01a',
    String code = 'EV04042003',
    String deviceId = '1',
  }) async {
    printDebug('Apis().checkTicket :  ${Apis().checkTicket}');

    // Setup request
    final request = Request(
      method: Method.GET,
      urlApi:
          '${Apis().checkTicket}?id=$id&code_event=$code&device_id=$deviceId',
    );

    // Connect api
    final response = await Service().getRequest(request, isSsoApi: true);

    // Handle error network
    if (response == null) {
      AppHelper.showToast(text: 'Quét thất bại . Lỗi hệ thống !');
      return false;
    }

    // Parse
    tickBloc = Tick.fromJson(response);

    if (response["code"] == 400 && response["type"] == 25) {
      return false;
    }

    if (response["code"] == 422 && response["type"] == 3) {
      return false;
    }

    return true;
  }
}
