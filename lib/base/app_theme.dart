import 'package:flutter/material.dart';
import 'package:qr_event/const/color.dart';

class AppTheme {
  final _lightTheme = ThemeData.light().copyWith(
    primaryColor: PRIMARY_COLOR,

    //
    appBarTheme: const AppBarTheme(
      color: PRIMARY_COLOR,
      iconTheme: IconThemeData(color: tabBarTextLabelColor),
    ),

    //
    tabBarTheme: const TabBarTheme(
      labelColor: PRIMARY_COLOR,
      unselectedLabelColor: tabBarTextLabelColor,
    ),

    //
    bottomAppBarTheme: const BottomAppBarTheme(color: CL_WHITE, elevation: 10),
    brightness: Brightness.light,
    indicatorColor: PRIMARY_COLOR,
    sliderTheme: SliderThemeData(
      activeTrackColor: CL_ACCENT_LIGHT,
      trackHeight: 3.0,
      tickMarkShape: const RoundSliderTickMarkShape(tickMarkRadius: 4),
      activeTickMarkColor: CL_ACCENT_LIGHT,
      inactiveTickMarkColor: CL_ACCENT_LIGHT.withOpacity(0.99),
      thumbColor: CL_ACCENT_LIGHT,
      thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 8.0),
      overlayShape: const RoundSliderOverlayShape(overlayRadius: 18.0),
    ),
    colorScheme: ColorScheme.fromSwatch().copyWith(secondary: PRIMARY_COLOR),
  );

  ThemeData get currentTheme => _lightTheme;
}
