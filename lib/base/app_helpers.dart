import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:qr_event/const/color.dart';
import 'package:qr_event/widgets/app_loading.dart';
import 'package:device_info/device_info.dart';

R lookup<R>(Map map, Iterable keys) {
  dynamic current = map;

  for (final key in keys) {
    if (current is Map) {
      current = current[key];
    } else if (key is int && current is Iterable && current.length > key) {
      current = current.elementAt(key);
    } else {
      return null;
    }
  }

  try {
    if (current is! R) {
      switch (R) {
        case DateTime:
          current = DateTime.tryParse(current?.toString() ?? '')?.toLocal();
          break;
        case num:
          current = num.tryParse(current?.toString() ?? '');
          break;
        case String:
          current = current?.toString();
          break;
      }
    }

    return current as R;
  } on dynamic catch (e) {
    printDebug('message: ${e.toString()}');
  }

  // return null as default
  return null;
}

void printDebug(String text) {
  debugPrint(text);
}

class AppHelper {
  static void showToast({String text = 'Messege', bool isError = true}) {
    Fluttertoast.showToast(
      msg: '$text',
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: isError ? PRIMARY_COLOR : SUCCESS_COLOR,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  static Future<void> showDialogForWidget(
    BuildContext context, {
    bool barrierDismissible = false,
    Widget widget,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (context) {
        return widget;
      },
    );
  }

  static void onLoading({BuildContext context, bool isShow = true}) {
    if (isShow) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AppLoadingIndicator();
        },
      );
    } else {
      Navigator.pop(context);
    }
  }

  static Map<String, dynamic> stringToMap(String text) {
    try {
      return jsonDecode(text);
    } catch (e) {
      return {};
    }
  }

  static Future<String> getDeviceID() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    String deviceId = '';

    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      deviceId = androidInfo.androidId;
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceId = iosInfo.identifierForVendor;
    }
    return deviceId;
  }
}
