import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:qr_event/const/color.dart';

Tick tickFromJson(String str) => Tick.fromJson(json.decode(str));

String tickToJson(Tick data) => json.encode(data.toJson());

Tick tickBloc = Tick();

class Tick {
  Tick({
    this.success,
    this.message,
    this.error,
    this.data,
    this.code,
    this.type,
  });

  final bool success;
  final String message;
  final String error;
  final Data data;
  final int code;
  final int type;

  bool get checkTickIsTrue => (success ?? false) && (code == 200);

  factory Tick.fromJson(Map<String, dynamic> json) => Tick(
        success: json["success"],
        message: json["message"],
        error: json["error"],
        data: Data.fromJson(json["data"]),
        code: json["code"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "message": message,
        "error": error,
        "data": data.toJson(),
        "code": code,
        "type": type,
      };
}

class Data {
  const Data({
    this.name,
    this.email,
    this.phone,
    this.status,
  });

  final String name;
  final String email;
  final String phone;
  final int status;

  String get getStatus => (status == 1) ? "Đã mua vé" : "Đã check-in";
  Color get getStatusColor => (status == 1) ? SUCCESS_COLOR : PRIMARY_COLOR;

  factory Data.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return Data();
    }

    return Data(
      name: json["name"] ?? '',
      email: json["email"] ?? '',
      phone: json["phone"] ?? '',
      status: json["status"] ?? 2,
    );
  }

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "phone": phone,
        "status": status,
      };
}
