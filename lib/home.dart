import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_event/base/app_helpers.dart';
import 'package:qr_event/const/color.dart';
import 'package:qr_event/model/tick.dart';
import 'package:qr_event/repository/sso_ticker_provider.dart';

import 'widgets/popup/popup_tick_infomation.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _isScaningQR = false;
  SSOTickerProvider _tickerProvider = SSOTickerProvider();
  Barcode result;
  QRViewController controller;
  int _scanSuccess = 0;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  Widget build(BuildContext context) {
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 200.0
        : 300.0;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset('assets/images/logo-tto.png', width: 120),
      ),
      body: Column(
        children: [
          // Camera QR
          Container(
            color: PRIMARY_COLOR.withOpacity(0.1),
            height: MediaQuery.of(context).size.width,
            width: double.maxFinite,
            child: _isScaningQR
                ? QRView(
                    key: qrKey,
                    onQRViewCreated: _onQRViewCreated,
                    overlay: QrScannerOverlayShape(
                      borderColor: PRIMARY_COLOR,
                      borderRadius: 10,
                      borderLength: 30,
                      borderWidth: 10,
                      cutOutSize: scanArea,
                    ),
                    onPermissionSet: (ctrl, p) =>
                        _onPermissionSet(context, ctrl, p),
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: SvgPicture.asset(
                          'assets/v2/icons/scan_barcode.svg',
                          height: 120,
                          color: tabBarTextLabelColor.withOpacity(0.5),
                        ),
                      ),
                      Text('\nBấm quét và đưa QR vào vị trí này'),
                    ],
                  ),
          ),

          // Emplty
          Expanded(child: Offstage()),

          // Button
          Center(
            child: Container(
              height: 140,
              width: 140,
              margin: EdgeInsets.only(bottom: 20, top: 20),
              child: FloatingActionButton(
                highlightElevation: 15,
                elevation: 15,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    getTextButtonScan.toUpperCase(),
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                backgroundColor: PRIMARY_COLOR,
                splashColor: PRIMARY_COLOR_TTS,
                onPressed: () => _onClickScanButton(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });

    controller.scannedDataStream.listen((scanData) {
      if (scanData != null && (scanData?.code?.isNotEmpty ?? false)) {
        if (_scanSuccess == 0) {
          _scanSuccess += 1;

          setState(() {
            _isScaningQR = !_isScaningQR;
          });

          _onHandleResuft(scanData.code);
        }
      }
    });
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('no Permission')),
      );
    }
  }

  void _onClickScanButton() {
    setState(() {
      _scanSuccess = 0;
      _isScaningQR = !_isScaningQR;
    });
  }

  Future<void> _onHandleResuft(String text) async {
    // Turn on loading
    AppHelper.onLoading(context: context, isShow: true);

    // Parse data from string
    final maps = AppHelper.stringToMap(text);
    final memberId = maps['member_id'] ?? '';
    final codeEvent = maps['code_event'] ?? '';

    // Get device info
    final deviceID = await AppHelper.getDeviceID();

    // Check ticker
    await _tickerProvider.checkTicker(
      id: memberId.toString(),
      code: codeEvent.toString(),
      deviceId: deviceID,
    );

    // Turn off loading
    AppHelper.onLoading(context: context, isShow: false);

    // Show info ticker
    AppHelper.showDialogForWidget(
      context,
      widget: TickInformation(tick: tickBloc),
    );
  }

  String get getTextButtonScan => _isScaningQR ? 'DỪNG' : 'QUÉT QR';

  // ignore: unused_element
  String _getMemberId(String text) {
    final int memberIdStart = text.indexOf(':') + 1;
    final int memberIdEnd = text.indexOf(',', memberIdStart);
    final String memberId = text.substring(memberIdStart, memberIdEnd).trim();

    return memberId;
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
